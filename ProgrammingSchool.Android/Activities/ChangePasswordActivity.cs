﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using ProgrammingSchool.Core.Helpers;

namespace ProgrammingSchool.Android
{
    [Activity(Label = "ChangePasswordActivity")]
    public class ChangePasswordActivity : Activity 
    {
        private EditText _editTextCurrentPassword;
        private EditText _editTextNewPassword;
        private EditText _editTextNewPasswordConfirmation;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_change_password);

            InitializeView();
        }

        private void InitializeView()
        {
            var button = FindViewById<Button>(Resource.Id.button_save);
            _editTextCurrentPassword = FindViewById<EditText>(Resource.Id.edittext_current_password);
            _editTextNewPassword = FindViewById<EditText>(Resource.Id.edittext_new_password);
            _editTextNewPasswordConfirmation = FindViewById<EditText>(Resource.Id.edittext_password_confirmation);

            button.Click += ChangeButtonOnClick;
        }

        private void ChangeButtonOnClick(object sender, EventArgs e)
        {
            VerifyChangePassword(_editTextCurrentPassword.Text, _editTextNewPassword.Text,
                _editTextNewPasswordConfirmation.Text);
        }

        private void VerifyChangePassword(string currentPassword, string newPassword, string newPasswordConfirmation)
        {
            var dialogBuilder = new AlertDialog.Builder(this)
                .SetTitle(Resource.String.provide_data)
                .SetNeutralButton(Resource.String.ok, (sender, args) => { });
                

            if (string.IsNullOrEmpty(currentPassword) || string.IsNullOrEmpty(newPassword) ||
                string.IsNullOrEmpty(newPasswordConfirmation))
            {
                dialogBuilder
                    .SetTitle(Resource.String.provide_data)
                    .Create()
                    .Show();
            }
            else if (currentPassword != Settings.Password)
            {
                dialogBuilder
                    .SetTitle(Resource.String.wrong_password)
                    .Create()
                    .Show();
            }
            else if (newPassword != newPasswordConfirmation)
            {
                dialogBuilder
                    .SetTitle(Resource.String.wrong_password_confirmation)
                    .Create()
                    .Show();
            }
            else
            {
                Settings.Password = newPassword;
                this.Finish();
            }
        }
    }
}