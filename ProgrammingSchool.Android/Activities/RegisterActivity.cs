﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using ProgrammingSchool.Core.Helpers;

namespace ProgrammingSchool.Android
{
    [Activity(Label = "School0603", Icon = "@mipmap/icon")]
    public class RegisterActivity : Activity
    {
        public const string EmailExtra = "email";
        
        private EditText _editTextEmail;
        private EditText _editTextPassword;
        private EditText _editTextPasswordConfirmation;
        private AlertDialog.Builder _alertDialogBuilder;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_register);
            InitializeView();
        }

        private void InitializeView()
        {
            var button = FindViewById<Button>(Resource.Id.button_login);
            _editTextEmail = FindViewById<EditText>(Resource.Id.edittext_email);
            _editTextPassword = FindViewById<EditText>(Resource.Id.edittext_password);
            _editTextPasswordConfirmation = FindViewById<EditText>(Resource.Id.edittext_password_confirmation);
            
            button.Click += LoginButtonOnClick;
        }

        private void LoginButtonOnClick(object sender, EventArgs e)
        {
            VerifyLoginData(_editTextEmail.Text, _editTextPassword.Text, _editTextPasswordConfirmation.Text);
        }

        private void VerifyLoginData(string email, string password, string passwordConfirmation)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(passwordConfirmation))
            {
                new AlertDialog.Builder(this)
                .SetTitle(Resource.String.wrong_login_data)
                .SetNeutralButton(Resource.String.ok, (sender, args) => { })
                .Create()
                .Show();
            }
            else
            {
                Settings.Email = email;
                Settings.Password = password;
                NavigateToMain();
            }
        }

        private void NavigateToMain()
        {
            var mainActivityIntent = new Intent(this, typeof(MainActivity));
            mainActivityIntent.PutExtra(EmailExtra, _editTextEmail.Text);
            StartActivity(mainActivityIntent);
        }
    }
}