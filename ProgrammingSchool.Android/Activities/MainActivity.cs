﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views;
using ProgrammingSchool.Core.ViewModels;

namespace ProgrammingSchool.Android
{
    [Activity(Label = "School0603", Icon = "@mipmap/icon")]
    public class MainActivity : MvxActivity<MainViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
        }
    }
}   