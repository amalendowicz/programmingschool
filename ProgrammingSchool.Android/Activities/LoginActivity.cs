﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using MvvmCross.Droid.Views;
using ProgrammingSchool.Android.Annotations;
using ProgrammingSchool.Android.Services;
using ProgrammingSchool.Core;
using ProgrammingSchool.Core.ViewModels;

namespace ProgrammingSchool.Android
{
    [Activity(Label = "School0603", Icon = "@mipmap/icon")]
    public class LoginActivity : MvxActivity<LoginViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_login);
        }
    }
}