﻿using Android.Content;
using ProgrammingSchool.Core.Services.Contracts;

namespace ProgrammingSchool.Android.Services
{
    public class DroidNavigationService : INavigationService
    {
        private readonly Context _context;

        public DroidNavigationService(Context context)
        {
            _context = context;
        }

        public void NavigateToMain(string email)
        {
            var mainActivityIntent = new Intent(_context, typeof(MainActivity));
            _context.StartActivity(mainActivityIntent);
        }

        public void NavigateToRegister(string email)
        {
            var registerIntent = new Intent(_context, typeof(RegisterActivity));
            registerIntent.PutExtra(RegisterActivity.EmailExtra, email);
            _context.StartActivity(registerIntent);
        }
    }
}