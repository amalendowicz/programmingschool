﻿using System;
using Android.App;
using Android.Content;
using MvvmCross.Platform.Droid.Platform;
using ProgrammingSchool.Android;
using ProgrammingSchool.Core.Services.Contracts;

public class DroidUserDialogs : IUserDialogs
{
    private readonly Context _activity;

    public DroidUserDialogs(IMvxAndroidCurrentTopActivity topActivity)
    {
        _activity = topActivity.Activity;
    }

    public void DisplayAlert(string title, string message)
    {
        new AlertDialog.Builder(_activity)
            .SetTitle(title)
            .SetMessage(message)
            .SetNeutralButton(Resource.String.ok, (sender, args) => { })
            .Create()
            .Show();
    }

    public void DisplayPrompt(string title, string message, string buttonPositiveText, string buttonNegativeText,
        Action positiveButtonAction)
    {
        new AlertDialog.Builder(_activity)
            .SetTitle(title)
            .SetMessage(message)
            .SetPositiveButton(buttonPositiveText, (sender, args) => positiveButtonAction?.Invoke())
            .SetNegativeButton(buttonNegativeText, (sender, args) => { })
            .Create()
            .Show();
    }
}