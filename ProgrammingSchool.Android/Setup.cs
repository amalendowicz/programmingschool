using System.Collections.Generic;
using System.Reflection;
using Android.Content;
using FFImageLoading.Cross;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using ProgrammingSchool.Core;
using ProgrammingSchool.Core.Services.Contracts;
using MvvmCross.Droid.Support.V7.RecyclerView;

namespace ProgrammingSchool.Android
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();
            Mvx.RegisterType<IUserDialogs, DroidUserDialogs>();
        }

        protected override IEnumerable<Assembly> AndroidViewAssemblies =>
        new List<Assembly>
        {
            typeof(MvxRecyclerView).Assembly,
            typeof(MvxCachedImageView).Assembly,
        };

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }
    }
}
