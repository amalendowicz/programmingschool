using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using ProgrammingSchool.Core.ViewModels;

namespace ProgrammingSchool.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.RegisterType<IAccountManagement, AccountManagement>();

            RegisterAppStart<LoginViewModel>();
        }
    }
}
