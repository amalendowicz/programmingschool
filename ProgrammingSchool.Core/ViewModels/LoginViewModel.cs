﻿using System.Collections.ObjectModel;
using MvvmCross.Core.ViewModels;

namespace ProgrammingSchool.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IAccountManagement _accountManagement;

        public LoginViewModel(IAccountManagement accountManagement)
        {
            _accountManagement = accountManagement;
        }

        public IMvxCommand LoginCommand => new MvxCommand(LoginAction);

        public bool IsLoginEnabled => !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Password);

        private string _email;

        public string Email
        {
            get { return _email; }
            set
            {
                if (SetProperty(ref _email, value))
                {
                    RaisePropertyChanged(() => IsLoginEnabled);
                }
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                if (SetProperty(ref _password, value))
                {
                    RaisePropertyChanged(() => IsLoginEnabled);
                }
            }
        }

        public void LoginAction()
        {
            //_accountManagement.Login(Email, Password);
            ShowViewModel<MainViewModel>();
        }
    }

    public class MainViewModel : MvxViewModel
    {
        public MainViewModel()
        {
            Photos = new ObservableCollection<Photo>
            {
                new Photo("https://8.allegroimg.com/original/016b87/188f41ac4edab7af2fc8330172b8"),
                new Photo("https://thumbs.img-sprzedajemy.pl/1000x901c/4f/1d/0e/nowosc-recznik-kapielowy-plazowy-kotek-konie-blonie-475280441.jpg"),
                new Photo("https://s3.pixers.pics/pixers/700/FO/35/27/50/13/700_FO35275013_8b6830d0464e71471067b4f5f60485d0.jpg")
            };
        }

        public ObservableCollection<Photo> Photos { get; set; }
    }

    public class Photo
    {
        public Photo(string photoUrl)
        {
            PhotoUrl = photoUrl;
        }

        public string PhotoUrl { get; set; }
    }
}
