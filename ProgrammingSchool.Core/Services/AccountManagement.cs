﻿using ProgrammingSchool.Core.Helpers;
using ProgrammingSchool.Core.Services.Contracts;

namespace ProgrammingSchool.Core
{
    public interface IAccountManagement
    {
        bool AccountExists(string email);

        bool IsPasswordCorrect(string email, string password);

        void ChangePassword(string email, string currentPassword, string password, string passwordConfirmation);

        void CreateAccount(string email, string password, string passwordConfirmation);

        void Login(string email, string password);
    }

    public class AccountManagement : IAccountManagement
    {
        private readonly IUserDialogs _userDialogs;

        public AccountManagement(IUserDialogs userDialogs)
        {
            _userDialogs = userDialogs;
        }

        public bool AccountExists(string email)
        {
            return Settings.Email == email;
        }

        public bool IsPasswordCorrect(string email, string password)
        {
            return Settings.Email == email && Settings.Password == password;
        }

        public void ChangePassword(string email, string currentPassword, string password, string passwordConfirmation)
        {
            if (password == passwordConfirmation)
            {
                if (AccountExists(email))
                {
                    if (Settings.Password == currentPassword)
                    {
                        Settings.Password = password;
                    }
                }
                else
                {
                    _userDialogs.DisplayAlert("Błąd", "Konto nie istnieje");
                }
            }
            else
            {
                _userDialogs.DisplayAlert("Błąd", "Hasło i potwierdzenie hasła nie są takie same");
            }
        }

        public void CreateAccount(string email, string password, string passwordConfirmation)
        {
            if (password == passwordConfirmation)
            {
                if (AccountExists(email))
                {
                    _userDialogs.DisplayAlert("Błąd", "Konto już istnieje");
                }
                else
                {
                    Settings.Email = email;
                    Settings.Password = password;
                }
            }
            else
            {
                _userDialogs.DisplayAlert("Błąd", "Podane hasła nie są takie same");
            }
        }

        public void Login(string email, string password)
        {
            if (AccountExists(email))
            {
                if (IsPasswordCorrect(email, password))
                {
                    //_navigationService.NavigateToMain(email);
                }
                else
                {
                    _userDialogs.DisplayAlert("Błąd", "Podane hasło nie jest prawidłowe");
                }
            }
            else
            {
                _userDialogs.DisplayPrompt("Konto nie istnieje", "Czy chcesz utworzyć konto", "Tak", "Nie", () =>
                {
                    //todo 
                });
            }
        }
    }
}