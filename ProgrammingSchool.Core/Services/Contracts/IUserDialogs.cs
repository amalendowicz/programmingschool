﻿using System;

namespace ProgrammingSchool.Core.Services.Contracts
{
    public interface IUserDialogs
    {
        void DisplayAlert(string title, string message);

        void DisplayPrompt(string title, string message, string buttonPositiveText, string buttonNegativeText, Action positiveButtonAction);
    }
}