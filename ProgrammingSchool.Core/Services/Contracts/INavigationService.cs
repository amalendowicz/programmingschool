﻿namespace ProgrammingSchool.Core.Services.Contracts
{
    public interface INavigationService
    {
        void NavigateToMain(string email);

        void NavigateToRegister(string email);
    }
}